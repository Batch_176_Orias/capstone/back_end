let express = require(`express`);
let mongoose = require(`mongoose`);
let dotenv = require(`dotenv`);
let userRoutes = require(`./routes/users`);
let productRoutes = require(`./routes/products`);
let orderRoutes = require(`./routes/orders`);
let cors = require(`cors`);

dotenv.config();

let application = express();
let db = process.env.CREDENTIALS;
let port = process.env.PORT;

application.use(express.json());
application.use(cors());
application.use(`/users`, userRoutes);
application.use(`/products`, productRoutes);
application.use(`/orders`, orderRoutes);

mongoose.connect(db);
mongoose.connection.once(`open`, () => {
	console.log(`MongoDB is now up and running`);
});

application.listen(port, () => {
	console.log(`Port ${port} is now up and running`);
});

application.get(`/`, (request, response) => {
	response.send(`This is connected to my reactjs. Check out: https://capstone-3-eight.vercel.app/`);
});