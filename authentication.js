let jwt = require('jsonwebtoken');
let encrypted = 'E-commerce';

module.exports.createToken = (request) =>{
	console.log(request);

	let requestData = {
		id: request._id,
		email: request.email,
		isAdmin: request.isAdmin
	}

	return jwt.sign(requestData, encrypted, {expiresIn: `1hr`});


}


module.exports.verifyToken = (request, response, next) => {
	console.log(request.headers.authorization);

	let token = request.headers.authorization;

	if(typeof token === `undefined`){
		return response.send({message: `No token`});
	}
	else{
		token = token.slice(7, token.length);
		console.log(token);

		jwt.verify(token, encrypted, (error, decodedToken) => {
			if (error) {
				return response.send({message: `Failed authentication`});
			} 
			else {
				console.log(decodedToken);
				request.user = decodedToken;
				console.log(decodedToken)

				next();
			}
		});
	}
};

	module.exports.verifyAdmin = (request, response, next) => {
		if(request.user.isAdmin){
			next();
		}
		else{
			return response.send({
				authorization: `Failed`,
				message: `Not admin`
			})
		}
	};

	module.exports.verifyNotAdmin = (request, response, next) => {
		if(request.user.isAdmin === false){
			next();
		}
		else{
			return response.send({
				authorization: `Failed`,
				message: `Not regular user`
			})
		}
	};