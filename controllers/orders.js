let Order = require(`../models/Order`);
let User = require(`../models/User`);
let Product = require(`../models/Product`);
let authentication = require(`../authentication`);
let mongoose = require(`mongoose`);
let moment = require('moment-timezone');

module.exports.getAllOrders = (authenticationId) => {
	return Order.find().sort({createdOn: -1}).then((success) => {
		if (success.length > 0) {
			return {message: `Record(s) found: ${success.length}`,
					result: success};
		}
		else {
			return {message: `No records found`};
		}
	}).catch((error) => {
		return error.message;
	});
};

/*module.exports.createOrder = async (request, requestId) => {
	console.log(request.productId);
	console.log(request.quantity);

	return Product.findById(request.productId).then((product) => {
		if (product) {
			let total = product.price * request.quantity;

			let order = {
				userId: requestId,
				totalAmount: total
			}

			let newOrder = new Order(order);

			newOrder.products.push(request);

			return newOrder.save().then((order) => {
				return {message: `Order created`,
						result: order};
			});
		} 
		else {
			return false;
		}
	}).catch((error) => {
		return error.message;
	});*/
	/*let isProductUpdated = await Order.findById(request.productId).then((success) => {
			let product = {
			productId: request.productId,
			quantity: request.quantity
		};
		success.products.push(product);

		return success.save().then((success) => {
			if (success) {
				return success
			} 
			else {
				return false;
			}
		}).catch((error) => {
			return error.message;
		});

		if(isProductUpdated !== true){
			return {message: isProductUpdated};
		}
	});

	if(isProductUpdated){
		return {message: `Added Successfully`};
	}*/
	/*return Product.findById(request).then((success) => {
		if (success) {
			return success
		} else {
			return false;
		}
	});*/
//};

module.exports.createOrder = (authenticationId) => {
	return User.findById(authenticationId).then((user) => {
		if(user){
			if(user.cart.length === 0){
				return {message: `User cart is empty`}
			}
			else{

				/*return {message: `User found`,
					details: user}		*/
				return Order.create({userId: user._id.toString(), orderDetails: user.cart}).then((createOrder) => {
					if(createOrder){
						let cartTotal = createOrder.orderDetails.reduce((accumulate, price) => {
							return accumulate + price.total;
						}, 0)
						return Order.updateOne({_id: createOrder._id}, {amountToPay: cartTotal}).then((orderPrice) => {
							if(orderPrice){
								return {message: `Order created`,
								details: createOrder}
							}
							else{
								return {message: `Order not created`}
							}
						})
					}
					else{
						return {message: `Order not created`}
					}
				})
			}
		}
		else{
			return {message: `User not found`}
		}
	}).catch((error) => {
		return error.message;
	})
};

module.exports.cancelOrder = (request, authenticationId) => {
	return Order.updateOne({
		$and:
		[
			{_id: request.orderId},
			{userId: authenticationId}
		]
	}, {$set: {status: `Canceled`}}).then((order) => {
		if(order){
			if(order.modifiedCount === 1){
				return {message: `Order has been canceled`,
						details: order}
			}
			else{
				return {message: `Order not found`}
			}
		}
		else{
			return {message: `Order not found`}
		}
	}).catch((error) => {
		return error.message;
	})
};

module.exports.updateOrderStatusAdmin = (request, authenticationId) => {
	return User.findById(authenticationId).then((user) => {
		if(user){
			return Order.updateOne({_id: request.orderId}, {$set: {status: request.status}}).then((order) => {
				if(order){
					return {message: `Order has been updated`,
							details: order}
				}
				else{
					return {message: `Order failed to update`}
				}
			})
		}
		else{
			return {message: `User not found`}
		}
	}).catch((error) => {
		return error.message;
	})
};

module.exports.searchOrdersAdmin = (request, authenticationId) => {
	return Order.find({
		$or: [
			{_id: request.id},
			{createdOn: {$lte: request.date}},
			{status: request.status}
		]
	}).then((order) => {
			console.log(moment.tz((order.createdOn), "Asia/Manila"))
			if (order) {
				return {message: `Record(s) found(${order.length} items)`,
						result: order};
			} 
			else {
				return {message: `Record not found`};
			}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.searchOrdersUser = (authenticationId) => {
	return Order.find({userId: authenticationId}).sort({'createdOn': -1}).then((order) => {
			if (order) {
				if(order.length === 0){
					return {message: `User's order is empty`}
				}
				else{
					return {message: `Record(s) found(${order.length} items)`,
							result: order};
				}
			} 
			else {
				return {message: `Record not found`};
			}
	}).catch((error) => {
		return error.message;
	});
};