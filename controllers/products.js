let Product = require(`../models/Product`);
let mongoose = require(`mongoose`);

module.exports.getAllProducts = () => {
	return Product.find({}).sort({createdOn: -1}).then((product) => {
		if (product.length > 0) {
			return {message: `Record(s) found: ${product.length}`,
					result: product};
		} 
		else {
			return {message: `No records found`};
		}
	});
};

//feature -- insensitive case search
module.exports.searchProducts = (request) => {
	return Product.find({
		$and: [
			{isActive: true},
			{
				$or: [
					{_id: request.id},
					{name: {$regex: `${request.name}`, $options: `$i`}},
					{description: {$regex: `${request.description}`, $options: `$i`}},
					{price: request.price}
				]
			}
		]
	}).then((product) => {
			if (product) {
				return {message: `Record(s) found(${product.length} found)`,
						result: product};
			} 
			else {
				return {message: `Record not found`};
			}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.searchProductsAdmin = (request, authenticationId) => {
	return Product.find({
		$or: [
			{_id: request.id},
			{name: {$regex: `${request.name}`, $options: `$i`}},
			{description: {$regex: `${request.description}`, $options: `$i`}},
			{price: request.price},
			{isActive: request.isActive}
		]
	}).then((product) => {
			if (product) {
				return {message: `Record(s) found(${product.length} found)`,
						result: product};
			} 
			else {
				return {message: `Record not found`};
			}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.createProduct = (authenticationId, request) => {

	let requestProduct = new Product ({
		name: request.name,
		description: request.description,
		price: request.price
	});

//additional feature -- validation of name existence
	return requestProduct.save(request).then((product) => {
		if (product) {
			return {message: `Product created`,
					product_id: product._id};
		} 
		else {
			return {message: `Account not created`};
		}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.updateProduct = (authenticationId, request) => {

	let requestProduct = {
		name: request.name,
		description: request.description,
		price: request.price
	};

	return Product.findByIdAndUpdate(request.id, requestProduct).then((product) => {
		if (product) {
			return {message: `Product ${product.name} has been updated to ${requestProduct.name}`};
		} 
		else {
			return {message: `Product not found`};
		}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.archiveProduct = (authenticationId, request) => {

	let requestProduct = {
		isActive: false
	};

	return Product.findOne({_id: mongoose.Types.ObjectId(request.id)}).then((products) => {
		if(products.isActive === false){
			return {message: `This product is already non-active`,
							details: products}
		}
		else if(products.isActive === true){
			return Product.updateOne({_id: mongoose.Types.ObjectId(request.id)}, requestProduct).then((archive) => {
				if(archive){
					return {message: `Product ${products._id} has been updated`,
							details: products}
				}
				else{
					return {message: `Product not found`}
				}
			})
		}
		else{
			return {message: `Product not found`}
		}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.activeProduct = (authenticationId, request) => {

	let requestProduct = {
		isActive: true
	};

	return Product.findOne({_id: mongoose.Types.ObjectId(request.id)}).then((products) => {
		if(products.isActive === true){
			return {message: `This product is already active`,
							details: products}
		}
		else if(products.isActive === false){
			return Product.updateOne({_id: mongoose.Types.ObjectId(request.id)}, requestProduct).then((archive) => {
				if(archive){
					return {message: `Product ${products._id} has been updated`,
							details: products}
				}
				else{
					return {message: `Product not found`}
				}
			})
		}
		else{
			return {message: `Product not found`}
		}
	}).catch((error) => {
		return error.message;
	});
};