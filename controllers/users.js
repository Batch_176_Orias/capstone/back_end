let User = require(`../models/User`);
let bcrypt = require(`bcrypt`);
let dotenv = require(`dotenv`);
let authentication = require(`../authentication`);
let mongoose = require(`mongoose`);
let Product = require('../models/Product');
let moment = require('moment-timezone');
let _ = require(`lodash`);

dotenv.config();	
let salt = Number(process.env.SALTROUNDS);

module.exports.getAllUsers = () => {
	return User.find({}).then((success) => {
		if (success.length > 0) {
			return {message: `Record(s) found: ${success.length}`,
					result: success};
		}
		else {
			return {message: `No records found`};
		}
	}).catch((error) => {
		return error.message;
	});
}

module.exports.getAllAdmins = () => {
	return User.find({isAdmin: true}).then((success) => {
		if (success.length > 0) {
			return {message: `Record(s) found: ${success.length}`,
					result: success};
		} 
		else {
			return {message: `No records found`};
		}
	});
};

module.exports.searchUser = (authenticationId, request) => {
	let userid = mongoose.Types.ObjectId(request.id);
	return User.find({
		$or: [
			{_id: userid},
			{email: request.email}
		]
	}, {password: 0, cart: 0, __v: 0, cartTotalAmount: 0}).then((user) => {
		if(user){
			if(user.length === 0){
				return {message: `User not found`}
			}
			else{
				return {message: `User found`,
					details: user}
			}
		}
		else{
			return {message: `User not found`}
		}
	}).catch((error) => {
		return error.message;
	});
}

module.exports.createUser = (request) => {

	let requestUser = new User({
		email: request.email,
		password: bcrypt.hashSync(request.password, salt)
	});

	//additional feature -- validation of email existence
	return User.find({email: request.email}).then((success) => {
		if (success.length == 0) {
			return requestUser.save(request).then((success) => {
				if (success) {
					return {message: `Account created`,
							account_id: success._id};
				} 
				else {
					return {message: `Account not created`};
				}
			}).catch((error) => {
				return error.message;
			});
		}
		else {
			return {message: `This email is already registered`};
		}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.loginUser = (request) => {
		return User.findOne({email: request.email}).then((success) => {
			if (success) {
				let isPassword = bcrypt.compareSync(request.password, success.password);
				if(isPassword){
					return {message: `Logged in`,
							account_id: success._id,
							Token_Created: authentication.createToken(success.toObject())};
				}
				else{
					return {message: `Email or password incorrect`};
				}
			} 
			else {
				return {message: `Email or password incorrect`};
			}
		}).catch((error) => {
			return error.message;
		});
	};

module.exports.getDetails = (request) => {
	return User.findById(request).then((user) => {
		if (user) {
			user.password = ``;
			return {message: `User's details`,
					details: user}
		} 
		else {
			return {message: `User not found`};
		}
	});
};

module.exports.setAdmin = (request) => {
	let setUser = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(request, setUser).then((user) => {
		if (user) {
			return {message: `User ${user.id} has been set to Admin`};
		} 
		else {
			return {message: `Set failed`};
		}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.setNonAdmin = (request) => {
	let setUser = {
		isAdmin: false
	}

	return User.findByIdAndUpdate(request, setUser).then((user) => {
		if (user) {
			return {message: `User ${user.id} has been set to Non-Admin`};
		} 
		else {
			return {message: `Set failed`};
		}
	}).catch((error) => {
		return error.message;
	});
};

module.exports.cart = (request, authenticationId) => {

	let prodId = mongoose.Types.ObjectId(request.productId);

	// User.checkout.push(newCheckout)

	return Product.findById(prodId).then((product) => {
		if(product){
			let newUser = {
					productId: prodId,
					productName: product.name,
					quantity: request.quantity,
					total: product.price * request.quantity
				};

				if(request.quantity < 1){
					return {message: `Quantity of item is less than minimum`}
				}
				else{
					return User.findByIdAndUpdate(authenticationId, {$push: {cart: newUser}}).then((user) => {
						if(user){
							
							let cartTotal = user.cart.reduce((acc, cart) => {
								return acc + cart.total
							}, newUser.total);
							
							return User.updateOne({_id: authenticationId}, {cartTotalAmount: cartTotal}).then((cartPrice) => {
								if(cartPrice){
									return {message: `User ${user.email} has added ${product.name} to cart(${request.quantity} item(s))`,
											details: user}
								}
							})

						}
						else{
							return {message: `Update failed`}
						}
					})
				}
			
		}
		else{
			return {message: `Product not found`}
		}
	}).catch((error) => {
		return error.message;
	})

}

module.exports.cartDetails = (authenticationId) => {

	// let userid = mongoose.Types.ObjectId(request.userid);

	return User.findById(authenticationId).then((user) => {
		if(user.cart.length === 0){
			return {message: `User cart is empty`
					};
		}
		else{
			return {message: `User ${user.email} cart found:`,
					details: user.cart,
					totalAmount: user.cartTotalAmount
					};
		}
	}).catch((error) => {
		return error.message;
	})
}

module.exports.removeCartItem = (request, authenticationId) => {

	let cartid = mongoose.Types.ObjectId(request.cartId);

	let removeCart = {
		_id: cartid
	}

	return User.updateOne({_id: authenticationId}, {$pull: {cart: removeCart}}).then((user) => {
		if(user.modifiedCount === 0){
			return {message: `Failed to be removed. Item not found`}	
		}
		else{
				return User.findById({_id: authenticationId}).then((userInfo) => {
					if(userInfo){
						let cartTotal = userInfo.cart.reduce((acc, price) => {
							return acc + price.total
						},0)
						console.log(cartTotal)
						return User.updateOne({_id: authenticationId}, {cartTotalAmount: cartTotal}).then((cartPrice) => {
							if(cartPrice){
								return {message: `Item No. ${cartid} has been removed`,
								details: user}
							}
						})
					}
				})

		}
	}).catch((error) => {
		return error.message;
	})
}

module.exports.removeAllItem = (authenticationId) => {
	return User.updateOne({_id: authenticationId}, {$set: {cart: []}}).then((user) => {
		if(user.modifiedCount === 0){
			return {message: `Failed to empty cart`}
		}
		else{

			return User.findById({_id: authenticationId}).then((userInfo) => {
				if(userInfo){
					let cartTotal = userInfo.cart.reduce((acc, price) => {
						return acc + price.total
					},0)
					console.log(cartTotal)
					return User.updateOne({_id: authenticationId}, {cartTotalAmount: cartTotal}).then((cartPrice) => {
						if(cartPrice){
							return {message: `Cart has been emptied`,
									details: user}
						}
					})
				}
			})			
				
		}
	}).catch((error) => {
		return error.message;
	})
}