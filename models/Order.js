let mongoose = require(`mongoose`);

let orderSchema = mongoose.Schema({
	userId: {
		type: String,
		required: [true, `User ID is required`]
	},
	
	orderDetails: [],

	amountToPay: {
		type: Number,
		default: 0.00
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	status: {
		type: String,
		default: `Pending`
	}
});

module.exports = mongoose.model(`Order`, orderSchema);