let mongoose = require(`mongoose`);

let userSchema = mongoose.Schema({
	email: {
		type: String,
		required: [true, `Email is required`]
	},

	password: {
		type: String,
		required: [true, `Password is required`]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	cart: [
		{
			productId: {
				type: String,
				required: [true, `Product ID is required`]
			},

			productName: {
				type: String,
				required: [true, `Product ID is required`]
			},

			quantity: {
				type: Number,
				required: [true, `Quantity is required`]
			},

			total: {
				type: Number
			},
			addedOn: {
				type: Date,
				default: new Date()
			}
		}
	],

	cartTotalAmount: {
		type: Number,
		default: 0.00
	}
});

module.exports = mongoose.model(`User`, userSchema);