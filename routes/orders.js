let express = require(`express`);
let ordersController = require(`../controllers/orders`);
let authentication = require(`../authentication`);

let router = express.Router();

router.get(`/all`, authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`GET method for orders is now up and running`);
	ordersController.getAllOrders(request.user.id).then((result) => {
		response.send(result);
	});
});

router.post(`/create-order`, authentication.verifyToken, authentication.verifyNotAdmin, (request, response) => {
	console.log(`POST method for orders is now up and running`);
	ordersController.createOrder(request.user.id).then((result) => {
			response.send(result);
	});
});

router.put(`/cancel-order`, authentication.verifyToken, (request, response) => {
	console.log(`PUT method for cancel order is now up and running`);
	ordersController.cancelOrder(request.body, request.user.id).then((result) => {
			response.send(result);
	});
});

router.put(`/update-order-admin`, authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`PUT method for update order admin is now up and running`);
	ordersController.updateOrderStatusAdmin(request.body, request.user.id).then((result) => {
			response.send(result);
	});
});

router.get(`/search-admin`, authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`PUT method for update order admin is now up and running`);
	ordersController.searchOrdersAdmin(request.body, request.user.id).then((result) => {
			response.send(result);
	});
});

router.get(`/orders-user`, authentication.verifyToken, authentication.verifyNotAdmin, (request, response) => {
	console.log(`PUT method for update order admin is now up and running`);
	ordersController.searchOrdersUser(request.user.id).then((result) => {
			response.send(result);
	});
});

module.exports = router;