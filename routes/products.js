let express = require(`express`);
let productsController = require(`../controllers/products`);
let authentication = require(`../authentication`);

let router = express.Router();

router.get(`/all`, (request, response) => {
	console.log(`GET method for products is now up and running`);
	productsController.getAllProducts().then((result) => {
		response.send(result);
	});
});

router.get(`/search`, (request, response) => {
	console.log(`GET method for search products is now up and running`);
	productsController.searchProducts(request.body).then((result) => {
		response.send(result);
	}).catch((error) => {
		return error.message;
	});
});

router.get(`/search-admin`, authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`GET method for search products for admin is now up and running`);
	productsController.searchProductsAdmin(request.body, request.user.id).then((result) => {
		response.send(result);
	}).catch((error) => {
		return error.message;
	});
});

router.post(`/create`, authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`POST method for products is now up and running`);
	productsController.createProduct(request.user.id, request.body).then((result) => {
		response.send(result);
	});
});

router.put(`/update`, authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`PUT method for products is now up and running`);
	productsController.updateProduct(request.user.id, request.body).then((result) => {
		response.send(result);
	});
});

router.put(`/archive`, authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`PUT method for archive is now up and running`);
	productsController.archiveProduct(request.user.id, request.body).then((result) => {
		response.send(result);
	});
});

router.put(`/active`, authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`PUT method for active product is now up and running`);
	productsController.activeProduct(request.user.id, request.body).then((result) => {
		response.send(result);
	});
});

module.exports = router;