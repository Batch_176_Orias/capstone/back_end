let express = require(`express`);
let usersController = require(`../controllers/users`);
let authentication = require(`../authentication`);

let router = express.Router();

router.get(`/all`, (request, response)=>{
	console.log(`GET method for users is now up and running`);
	usersController.getAllUsers().then((result) => {
		response.send(result);
	});
});

router.get(`/admin`, (request, response) => {
	console.log(`GET method for admins is now up and running`);
	usersController.getAllAdmins().then((result) => {
		response.send(result);
	});
});

router.get(`/details`, authentication.verifyToken, (request, response) => {
		usersController.getDetails(request.user.id).then((result) => {
			response.send(result);
		});
});

router.get('/searchUser', authentication.verifyToken, authentication.verifyAdmin, (request, response) => {
	console.log(`GET method for search user is now up and running`);
	usersController.searchUser(request.user.id, request.body).then((result) => {
		response.send(result);
	})
});

router.post(`/create`, (request, response)=>{
	console.log(`POST method for users is now up and running`);
	usersController.createUser(request.body).then(result=>{
		response.send(result);
	});
});

router.post(`/login`, (request, response) => {
	console.log(`POST method for login is now up and running`);
	usersController.loginUser(request.body).then((result) => {
		response.send(result);
	});
});

router.put(`/admin`, authentication.verifyToken, authentication.verifyAdmin, (request, response)=>{
	console.log(`PUT method for set admin is now up and running`);
	usersController.setAdmin(request.body.id).then((result) => {
		response.send(result);
	});
});

router.put(`/non-admin`, authentication.verifyToken, authentication.verifyAdmin, (request, response)=>{
	console.log(`PUT method for non admin is now up and running`);
	usersController.setNonAdmin(request.body.id).then((result) => {
		response.send(result);
	});
});

router.put('/cart', authentication.verifyToken, authentication.verifyNotAdmin, (request, response) => {
	console.log(`PUT method for user cart is now up and running`);
	usersController.cart(request.body, request.user.id).then((result) => {
		response.send(result);
	})
});

router.get('/cart-details', authentication.verifyToken, authentication.verifyNotAdmin, (request, response) => {
	console.log(`GET method for cart details is now up and running`);
	usersController.cartDetails(request.user.id).then((result) => {
		response.send(result);
	})
});

router.put(`/remove-cart-item`, authentication.verifyToken, authentication.verifyNotAdmin, (request, response) => {
	console.log(`PUT method for remove cart item is now up and running`);
	usersController.removeCartItem(request.body, request.user.id).then((result) => {
		response.send(result);
	});
});

router.put('/remove-all-item', authentication.verifyToken, authentication.verifyNotAdmin, (request, response) => {
	console.log(`PUT method for remove all item is now up and running`);
	usersController.removeAllItem(request.user.id).then((result) => {
		response.send(result);
	})
});

router.delete(`/remove`, (request, response)=>{
	response.send(`DELETE method for users is now up and running`);
});


module.exports = router;